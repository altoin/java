package com.company;

import com.company.Files.ManagerOrderFile;
import com.company.Files.ManagerOrderJSON;

public class Main {

    public static Orders<Order> orders = new Orders();
    private static String nameOfFileBin = "test.bin";
    private static String nameOfFileJson = "test.json";

    public static void main(String[] args) {
        Thread generateThread = new Thread(new Generate());
        Thread checkProcessingThread = new Thread(new CheckProcessing());
        Thread checkFinishedThread = new Thread(new CheckFinished());
        ManagerOrderFile bin = new ManagerOrderFile();
        ManagerOrderJSON json = new ManagerOrderJSON();
        json.readAll(nameOfFileJson);

        generateThread.start();
        checkFinishedThread.start();
        checkProcessingThread.start();

        //bin.saveAll(nameOfFileBin);
        //json.saveAll(nameOfFileJson);
        //bin.readAll(nameOfFileBin);
        orders.showAll();


        /*for (int i = 0; i < Integer.parseInt(args[0]); i++){
            if (args[1].equals("Tea"))
                Prod[i] = new CTea();
            if (args[1].equals("Coffee"))
                Prod[i] = new CCoffee();
            Prod[i].create();

            cart.add(Prod[i]);
        }

        orders.purchase(cart, cred);

        orders.showAll();

        cart.delete(Prod[0]);*/

        /*for (int i = 0; i < Integer.parseInt(args[0]); i++){
            Prod[i].read();
        }*/
    }
}
