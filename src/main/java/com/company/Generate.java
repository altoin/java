package com.company;

public class Generate implements Runnable{
    @Override
    public void run() {
        int i = 1;
        while (true){
            Products prod;
            if (i % 2 == 0) {
                prod = new CTea();
            }
            else{
                prod = new CCoffee();
            }
            Credentials cred = new Credentials();
            ShoppingCart<Products> cart = new ShoppingCart();

            prod.create();
            cart.add(prod);
            Main.orders.purchase(cart, cred);
            System.out.println("Created " + i + " orders");
            //Main.orders.showAll();
            i++;
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
