package com.company;

import java.io.Serializable;
import java.util.Random;
import java.util.UUID;

public class Credentials implements Serializable {

    public UUID ID;
    public String name;
    public String surname;
    public String patronymic;
    public String eMail;

    public Credentials() {
        int i = 0;
        Random randNumber = new Random();
        int count = randNumber.nextInt(7) + 1;
        StringBuffer strName = new StringBuffer();
        StringBuffer strSurname = new StringBuffer();
        StringBuffer strPatronymic = new StringBuffer();
        StringBuffer streMail = new StringBuffer();

        for (; i < count; i++) {
            strName.append((char)(randNumber.nextInt(26) + 'a'));
            strSurname.append((char)(randNumber.nextInt(26) + 'a'));
            strPatronymic.append((char)(randNumber.nextInt(26) + 'a'));
            streMail.append((char)(randNumber.nextInt(26) + 'a'));

        }

        ID = UUID.randomUUID();
        this.name = strName.toString();
        this.surname = strSurname.toString();
        this.patronymic = strPatronymic.toString();
        this.eMail = streMail.toString();
    }

    public Credentials(String name, String surname, String patronymic, String eMail) {
        ID = UUID.randomUUID();
        this.name = name;
        this.surname = surname;
        this.patronymic = patronymic;
        this.eMail = eMail;
    }

    public void show() {
        System.out.println("ID credential - " + ID);
        System.out.println("name credential - " + name);
        System.out.println("surname credential - " + surname);
        System.out.println("patronymic credential - " + patronymic);
        System.out.println("eMail credential - " + eMail);

    }
}
