package com.company;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.io.Serializable;
import java.util.Random;
import java.util.UUID;
import java.util.Scanner;
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "ID",
        "Name",
        "Price",
        "Country"
})
@JsonDeserialize(as = CCoffee.class)
public abstract class Products implements ICrudAction, Serializable {
    public UUID ID;
    public String Name;
    public double Price;
    public String Country;
    public static int Count;

    @Override
    public void create() {
        int i = 0;
        Random randNumber = new Random();
        int count = randNumber.nextInt(7) + 1;
        Price = randNumber.nextDouble() * 100;
        StringBuffer strName = new StringBuffer();
        StringBuffer strCountry = new StringBuffer();

        for (; i < count; i++) {
            strName.append((char)(randNumber.nextInt(26) + 'a'));
            strCountry.append((char)(randNumber.nextInt(26) + 'a'));
        }
        Name = strName.toString();
        Country = strCountry.toString();
        ID = UUID.randomUUID();
        Count++;
    }

    @Override
    public void read() {
        System.out.println("Name is: " + Name);
        System.out.println("Price is: " + Price);
        System.out.println("ID is: " + ID);
        System.out.println("Country is: " + Country);
    }

    @Override
    public void update() {
        create();

        Scanner in = new Scanner(System.in);

        System.out.println("Input product name");
        Name = in.nextLine();

        System.out.println("Input product price");
        Price = in.nextDouble();
        in.nextLine();

        System.out.println("Input product country");
        Country = in.nextLine();
    }

    public UUID getID() {
        return ID;
    }

    @Override
    public void delete() {
        Count--;
        Name = null;
        Price = 0.0;
        Country = null;
        ID = null;
    }

}
