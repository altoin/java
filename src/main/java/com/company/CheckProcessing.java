package com.company;

public class CheckProcessing extends ACheck{

    public void run() {
        while(true){
            try {
            Main.orders.checkProcessing();
            System.out.println("I'm checking for processing");
                Thread.sleep(1200);
            } catch (InterruptedException e) {
                System.out.println("Some problem with processing thread");
                e.printStackTrace();
            }
        }
    }
}
