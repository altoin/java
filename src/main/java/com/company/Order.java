package com.company;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;
import java.sql.Time;
import java.util.Random;

public class Order implements Serializable {

    public ShoppingCart cart;
    public STATUS status;
    public long timeForWait;
    public Credentials user;
    @JsonIgnore
    public Time timeOfCreation;

    public Order(){

    }

    public void setStatus(STATUS status) {
        this.status = status;
    }

    public Order(ShoppingCart cart, Credentials user) {

        Random rand = new Random();

        this.user = user;
        this.cart = cart;
        status = STATUS.PROCESSING;
        timeOfCreation = new Time(System.currentTimeMillis());
        timeForWait = rand.nextInt(1000);
    }

    public Time getTimeOfCreation() {
        return timeOfCreation;
    }

    public long getTimeForWait() {
        return timeForWait;
    }

    public STATUS getStatus() {
        return status;
    }

    public ShoppingCart getCart() {
        return cart;
    }

    public Credentials getUser() {
        return user;
    }
}
