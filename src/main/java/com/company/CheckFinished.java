package com.company;

public class CheckFinished extends ACheck{

    public void run() {
        while(true){
            Main.orders.check();
            System.out.println("I'm checking for finished");
            try {
                Thread.sleep(1500);
            } catch (InterruptedException e) {
                System.out.println("Some problem with finished thread");
                e.printStackTrace();
            }
        }
    }
}
