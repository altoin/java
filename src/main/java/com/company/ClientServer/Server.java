package com.company.ClientServer;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {
    public static void main(String[] args) {
        try {
            System.out.println("Server is running");
            int port = 3333;
            // создание серверного сокета
            ServerSocket ss = new ServerSocket(port);
            // Ждет клиентов и для каждого создает отдельный поток
            while (true) {
                Socket s = ss.accept();
                ServerConnectionProcessor p =
                        new ServerConnectionProcessor(s);
                p.start();
            }
        } catch (Exception e) {
            System.out.println(e);
        }
    }
}

class ServerConnectionProcessor extends Thread {
    private Socket sock;

    public ServerConnectionProcessor(Socket s) {
        sock = s;
    }

    public void run() {
        try {
            // Получает запрос
            DataInputStream inStream = new DataInputStream(
                    sock.getInputStream());
            int operationId = inStream.readInt();
            int arg1 = inStream.readInt();
            int arg2 = inStream.readInt();
            // Выполняет расчет
            int result = 0;
            if (operationId == 0) {
                result = arg1 + arg2;
            } else if (operationId == 1) {
                result = arg1 * arg2;
            }
            // Отправляет ответ
            DataOutputStream outStream = new DataOutputStream(
                    sock.getOutputStream());
            outStream.writeInt(result);
            System.out.println("It's working");
            // Подождем немного и завершим поток
            sleep(1000);
            inStream.close();
            outStream.close();
            sock.close();
        } catch (Exception e) {
            System.out.println(e);
        }
    }
}
