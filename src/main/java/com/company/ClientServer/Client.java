package com.company.ClientServer;

import com.company.CTea;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.net.Socket;

public class Client {
    public static void main(String[] args) throws Exception {
        // Имя хоста и номер порта
        String host = "localhost";
        int port = 3333;
        // Протокол  передачи
        // Запрос (3 целых чила): [операция][аргумент 1][аргумент 2]
        // Ответ (1 целое число): [результат]
        // Операции: 0 - сложение, 1 - умножение
        CTea tea = new CTea();
        tea.create();
        int operation = 0;
        int arg1 = 5;
        int arg2 = 2;
        try {
            System.out.println("Client is running");
            //  запрос клиента на соединение
            Socket sock = new Socket(host, port);
            // Исходящий поток
            DataOutputStream outStream = new DataOutputStream(
                    sock.getOutputStream());
            // Отправляем запрос и аргументы
            outStream.writeInt(operation);
            outStream.writeInt(arg1);
            outStream.writeInt(arg2);
            // Входящий поток
            DataInputStream inStream = new DataInputStream(
                    sock.getInputStream());
            int d = inStream.readInt();
            System.out.println("Result is " + d);
            // Завершаем потоки и закрываем сокет
            inStream.close();
            outStream.close();
            sock.close();
        } catch (Exception e) {
            System.err.println(e);
        }
    }
}

