package com.company.ClientServer;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

public class UDP_Send {
    private String host;
    private int port;

    public UDP_Send(String host, int port) {
        this.host = host;
        this.port = port;
    }

    public void sendMessage(String mes) {
        try {
            byte[] data = mes.getBytes();
            InetAddress addr = InetAddress.getByName(host);
            DatagramPacket pack = new DatagramPacket(data, data.length, addr, port);
            DatagramSocket ds = new DatagramSocket();
            ds.send(pack);
            ds.close();
        } catch (Exception e) {
            System.err.println(e);
        }
    }

    public static void main(String[] args) {
        System.out.println("Sender is running");
        UDP_Send sndr = new UDP_Send("localhost", 3333);
        for (int i = 0; i < 10; i++)
            sndr.sendMessage("test message " + i);
    }
}