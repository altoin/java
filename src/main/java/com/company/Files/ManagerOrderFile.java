package com.company.Files;

import com.company.Order;

import java.util.UUID;
import java.io.*;

import static com.company.Main.orders;

public class ManagerOrderFile extends AManagerOrder{

    @Override
    public void readAll(String file) {
        try(FileInputStream fis = new FileInputStream(file)){
            ObjectInputStream oin = new ObjectInputStream(fis);
            Order ord = (Order) oin.readObject();
            orders.purchase(ord.getCart(), ord.getUser());
            System.out.println(fis.available());
            while (ord != null){
                ord = (Order) oin.readObject();
                orders.purchase(ord.getCart(), ord.getUser());
            }
            oin.close();
            System.out.println("Reading done");
        } catch (Exception e) {
            System.out.println(e.toString());
            System.out.println("Some problem with BIN file");
        }
    }

    @Override
    public void readById(String file, UUID ID) {
        try(FileInputStream fis = new FileInputStream(file)){
            ObjectInputStream oin = new ObjectInputStream(fis);
            Order ord = (Order) oin.readObject();
            System.out.println(fis.available());
            while ((ord != null) && (ord.getCart().search(ID))){
                ord = (Order) oin.readObject();
            }
            orders.purchase(ord.getCart(), ord.getUser());
            oin.close();
            System.out.println("Reading by ID done");
        } catch (Exception e) {
            System.out.println(e.toString());
            System.out.println("Some problem with reading from file by ID");
        }
    }

    @Override
    public void saveAll(String file) {
        try(FileOutputStream fos = new FileOutputStream(file)){
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            for(Order ord : orders.getOrder()) {
                oos.writeObject(ord);
            }
            oos.flush();
            oos.close();
            System.out.println("Saved");
        } catch (Exception e) {
            System.out.println(e.toString());
            System.out.println("Some problem with save all");
        }
    }

    @Override
    public void saveById(String file, UUID ID) {
        try(FileOutputStream fos = new FileOutputStream(file)){
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            for(Order ord : orders.getOrder()) {
                if (ord.getCart().search(ID)) {
                    oos.writeObject(ord);
                }
            }
            oos.flush();
            oos.close();
            System.out.println("Saved");
        } catch (Exception e) {
            //System.out.println(e.toString());
            System.out.println("Some problem with save by ID");
        }
    }
}
