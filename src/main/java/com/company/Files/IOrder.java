package com.company.Files;

import java.util.UUID;

public interface IOrder {
    void saveById(String file, UUID ID);
    void saveAll(String file);
    void readById(String file, UUID ID);
    void readAll(String file);
}
