package com.company.Files;

import com.company.Order;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.*;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.UUID;

import static com.company.Main.orders;

public class ManagerOrderJSON extends AManagerOrder{

    @Override
    public void saveById(String file, UUID ID) {
        try{
            FileOutputStream fos = new FileOutputStream(file);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            for(Order ord : orders.getOrder()) {
                if (ord.getCart().search(ID)) {
                    Gson gson = new Gson();
                    String json = gson.toJson(ord);
                    System.out.println(json);
                    oos.writeObject(json);
                }
            }
            oos.flush();
            oos.close();
            System.out.println("Json saved");
        } catch (Exception e) {
            System.out.println(e.toString());
            System.out.println("Some problem with save all JSON");
        }
    }

    @Override
    public void saveAll(String file) {
        try{
            FileWriter wr = new FileWriter(file);
            for(Order ord : orders.getOrder()) {
                Gson gson = new GsonBuilder().setPrettyPrinting().create();
                System.out.println(gson.toJson(ord));
                wr.write(gson.toJson(ord));
            }
            wr.close();
            System.out.println("Json saved");
        } catch (Exception e) {
            System.out.println(e.toString());
            System.out.println("Some problem with save all JSON");
        }
    }

    @Override
    public void readById(String file, UUID ID) {
        try{
            byte[] jsonData = Files.readAllBytes(Paths.get(file));
            ArrayList<Order> ord;
            ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
            ord = objectMapper.readValue(jsonData, new TypeReference<ArrayList<Order>>(){});
            for(Order o : ord) {
                if (o.getCart().search(ID))
                    orders.purchase(o.getCart(),o.getUser());
            }
        } catch (Exception e){
            System.out.println("!!!!!!!!ERROR!!!!!!!!ERROR!!!!!!!ERROR!!!!!!");
            System.out.println(e.toString());
        }
    }

    @Override
    public void readAll(String file) {
        try{

            byte[] jsonData = Files.readAllBytes(Paths.get(file));
            ArrayList<Order> ord;
            ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
            ord = objectMapper.readValue(jsonData, new TypeReference<ArrayList<Order>>(){});
            for(Order o : ord) {
                orders.purchase(o.getCart(),o.getUser());
            }
       } catch (Exception e){
            System.out.println("!!!!!!!!ERROR!!!!!!!!ERROR!!!!!!!ERROR!!!!!!");
            System.out.println(e.toString());
        }
    }
}
