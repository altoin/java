package com.company;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.io.Serializable;
import java.util.Random;
import java.util.Scanner;
import java.util.UUID;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "ID",
        "Name",
        "Price",
        "Country"
})
@JsonDeserialize(as=CCoffee.class)
public class CCoffee extends Products implements Serializable {

    @JsonIgnoreProperties
    public String TypeOfBeans;

    @Override
    public void create() {
        int i = 0;
        Random randNumber = new Random();
        int count = randNumber.nextInt(7) + 1;
        StringBuffer str = new StringBuffer();

        for (; i < count; i++) {
            str.append((char)(randNumber.nextInt(26) + 'a'));
        }
        TypeOfBeans = str.toString();
        super.create();
    }

    @Override
    public void read() {
        super.read();
        System.out.println("Type of beans: " + TypeOfBeans);
    }

    @Override
    public void update() {
        super.update();

        Scanner in = new Scanner(System.in);

        System.out.println("Input product type of beans");
        TypeOfBeans = in.nextLine();
    }

    @Override
    public void delete() {
        super.delete();
        TypeOfBeans = null;
    }
}
