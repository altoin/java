package com.company;

import com.fasterxml.jackson.annotation.*;

import java.io.Serializable;
import java.util.*;
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "IDList",
        "cart"
})
public class ShoppingCart<T extends Products> implements Serializable {

    @JsonProperty("IDList")
    private HashSet<UUID> IDList = new HashSet<>();
    @JsonProperty("cart")
    private List<T> cart = new LinkedList<>();

    @JsonProperty("IDList")
    public HashSet<UUID> getIDList() {
        return IDList;
    }
    @JsonProperty("cart")
    public List<T> getCart() {
        return cart;
    }

    public ShoppingCart(){

    }

    public void add(T el) {
        IDList.add(el.getID());
        cart.add(el);
    }
    @JsonProperty("IDList")
    public void setIDList(HashSet<UUID> IDList) {
        this.IDList = IDList;
    }
    @JsonProperty("cart")
    public void setCart(List<T> cart) {
        this.cart = cart;
    }

    public void delete(T el) {
        IDList.remove(el.getID());
    }

    public boolean search(UUID id) {
        return IDList.contains(id);
    }

    public void showAll() {

        Iterator<T> i = cart.iterator();

        while ( i.hasNext() ){
            i.next().read();
        }
    }
}
