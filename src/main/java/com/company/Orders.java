package com.company;

import java.io.Serializable;
import java.sql.Time;
import java.util.*;

public class Orders<T extends Order> implements Serializable {

    public List<T> order = new LinkedList<>();
    public Map<Time, Order> orderByTyme = new HashMap();

    public Orders(){

    }

    public void purchase(ShoppingCart cart, Credentials user) {

        Order ord = new Order(cart, user);
        orderByTyme.put(ord.getTimeOfCreation(), ord);
        order.add( (T) new Order(cart, user));
    }

    public List<T> getOrder() {
        return order;
    }

    public void check() {

        //Iterator<T> ord = order.iterator();
        for (T ord : order){
        //while (ord.hasNext()) {
            //T elem = ord.next();
            synchronized (ord){
                if ((ord.getStatus().equals(STATUS.FINISHED))) {
                    //ord.getUser().show();
                    System.out.println("Removed");
                    orderByTyme.remove(ord.getTimeOfCreation());
                    order.remove(ord);
                }
            }
        }
    }

    public void checkProcessing() {
        for (T ord : order){
        //Iterator<T> ord = order.iterator();
        //while (ord.hasNext()) {

            Time t = new Time(System.currentTimeMillis());
            //T elem = ord.next();
            synchronized (ord) {
                if ((ord.getStatus().equals(STATUS.PROCESSING)) && (ord.getTimeOfCreation().getTime() + ord.getTimeForWait() > t.getTime())) {
                    //ord.getUser().show();
                    System.out.println("Update");
                    ord.setStatus(STATUS.FINISHED);
                }
            }
        }
    }

    public void showAll() {

        Iterator<T> ord = order.iterator();

        while (ord.hasNext()) {
            Order tmp = ord.next();
            tmp.getCart().showAll();
            tmp.getUser().show();
        }
    }
}
