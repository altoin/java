package com.company;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.io.Serializable;
import java.util.Scanner;
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "Type",
        "ID",
        "Name",
        "Price",
        "Country"
})
@JsonDeserialize(as=CTea.class)
public class CTea extends Products implements Serializable {
    @JsonProperty("Type")
    protected String Type;

    @Override
    public void create() {
        super.create();
        Type = "green";
    }

    @Override
    public void read() {
        super.read();
        System.out.println("Type of pack is: " + Type);
    }

    @Override
    public void update() {
        super.update();

        Scanner in = new Scanner(System.in);

        System.out.println("Input type of pack");
        Type = in.nextLine();
    }

    @Override
    public void delete() {
        super.delete();
        Type = null;
    }
}
